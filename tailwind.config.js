module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        green: {
          30: '#E6FFFA',
          250: '#81E6D9',
          450: '#319795'
        },
        blue: {
          30: '#EBF4FF',
          550: '#3182CE'
        }
      },
      textColor: {
        'button': '#319795',
        'primary': '#4A5568',
        'white': '#fff'
      },
    },
    fontSize: {
      'sm': '14px',
    },
    fontFamily: {
      sans: ["Lato"],
      serif: ["Lato"],
      mono: ["Lato"],
      display: ["Lato"],
      body: ["Lato"]
    }
  },
  variants: {
    extend: {
      textColor: ['active']
    },
  },
  plugins: [],
}
